#pragma once
#include "../../Global/Global.h"
#include "../../DataBase/DataBase.h"
#include "../User.h"
#include "AdminPanel.h"
#include "Editor/EditorForm.h"
#include <vector>

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� AdminPanelForm
	/// </summary>
	public ref class AdminPanelForm : public System::Windows::Forms::Form
	{
	public:
		AdminPanelForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
			_users = new DataBase<User>(userFile);
		}
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~AdminPanelForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ searchField;
	protected:

	protected:



	private: System::Windows::Forms::Button^ resetButton;
	private: System::Windows::Forms::Button^ editButton;
	private: System::Windows::Forms::ListBox^ userList;

	private:




	protected:

	private:
		DataBase<User>* _users;
	private: System::Windows::Forms::Button^ searchButton;
		   /// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->userList = (gcnew System::Windows::Forms::ListBox());
			this->searchField = (gcnew System::Windows::Forms::TextBox());
			this->resetButton = (gcnew System::Windows::Forms::Button());
			this->editButton = (gcnew System::Windows::Forms::Button());
			this->searchButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// userList
			// 
			this->userList->FormattingEnabled = true;
			this->userList->ItemHeight = 18;
			this->userList->Location = System::Drawing::Point(14, 65);
			this->userList->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->userList->Name = L"userList";
			this->userList->Size = System::Drawing::Size(302, 148);
			this->userList->TabIndex = 0;
			this->userList->SelectedIndexChanged += gcnew System::EventHandler(this, &AdminPanelForm::userList_SelectedIndexChanged);
			// 
			// searchField
			// 
			this->searchField->Location = System::Drawing::Point(14, 29);
			this->searchField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->searchField->Name = L"searchField";
			this->searchField->Size = System::Drawing::Size(116, 26);
			this->searchField->TabIndex = 1;
			// 
			// resetButton
			// 
			this->resetButton->Location = System::Drawing::Point(229, 28);
			this->resetButton->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->resetButton->Name = L"resetButton";
			this->resetButton->Size = System::Drawing::Size(87, 26);
			this->resetButton->TabIndex = 3;
			this->resetButton->Text = L"��������";
			this->resetButton->UseVisualStyleBackColor = true;
			this->resetButton->Click += gcnew System::EventHandler(this, &AdminPanelForm::resetButton_Click);
			// 
			// editButton
			// 
			this->editButton->Enabled = false;
			this->editButton->Location = System::Drawing::Point(348, 65);
			this->editButton->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->editButton->Name = L"editButton";
			this->editButton->Size = System::Drawing::Size(132, 26);
			this->editButton->TabIndex = 4;
			this->editButton->Text = L"�������������";
			this->editButton->UseVisualStyleBackColor = true;
			this->editButton->Click += gcnew System::EventHandler(this, &AdminPanelForm::editButton_Click);
			// 
			// searchButton
			// 
			this->searchButton->Location = System::Drawing::Point(136, 28);
			this->searchButton->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->searchButton->Name = L"searchButton";
			this->searchButton->Size = System::Drawing::Size(87, 26);
			this->searchButton->TabIndex = 5;
			this->searchButton->Text = L"�����";
			this->searchButton->UseVisualStyleBackColor = true;
			this->searchButton->Click += gcnew System::EventHandler(this, &AdminPanelForm::searchButton_Click);
			// 
			// AdminPanelForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 18);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->ClientSize = System::Drawing::Size(495, 249);
			this->Controls->Add(this->searchButton);
			this->Controls->Add(this->editButton);
			this->Controls->Add(this->resetButton);
			this->Controls->Add(this->searchField);
			this->Controls->Add(this->userList);
			this->Font = (gcnew System::Drawing::Font(L"Open Sans", 10));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"AdminPanelForm";
			this->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->Text = L"������ ��������������";
			this->Load += gcnew System::EventHandler(this, &AdminPanelForm::AdminPanelForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void AdminPanelForm_Load(System::Object^ sender, System::EventArgs^ e) {
		_users->Print(this->userList);
	}
private: System::Void userList_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	this->editButton->Enabled = true;
}
private: System::Void editButton_Click(System::Object^ sender, System::EventArgs^ e) {
	int selected = this->userList->SelectedIndex;
	vector<unsigned long int> userTableID = _users->GetTableItems();
	State::EditorForm^ editorPanel = gcnew EditorForm(userTableID[selected]);
	editorPanel->ShowDialog(this);
}
private: System::Void searchButton_Click(System::Object^ sender, System::EventArgs^ e) {
	string search = convertSysStringToChar(this->searchField->Text);
	_users->Print(this->userList, search);
}
private: System::Void resetButton_Click(System::Object^ sender, System::EventArgs^ e) {
	this->searchField->Text = gcnew System::String("");
	_users->Print(this->userList);
}
};
}
