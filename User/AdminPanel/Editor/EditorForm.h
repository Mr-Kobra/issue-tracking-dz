#pragma once
#include "../../../Global/Global.h"
#include "../../../DataBase/DataBase.h"
#include "../../User.h"
#include <string>

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� EditorForm
	/// </summary>
	public ref class EditorForm : public System::Windows::Forms::Form
	{
	public:
		EditorForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}
		EditorForm(unsigned long int ID) {
			InitializeComponent();
			_ID = ID;
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~EditorForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ usernameField;
	private: System::Windows::Forms::Label^ usernameLabel;
	private: System::Windows::Forms::Label^ positionLabel;
	private: System::Windows::Forms::TextBox^ positionField;
	private: System::Windows::Forms::Label^ levelLabel;
	private: System::Windows::Forms::TextBox^ lavelField;
	protected:






	private: System::Windows::Forms::Label^ passLabel;
	private: System::Windows::Forms::TextBox^ passField;
	private: System::Windows::Forms::Label^ nameLabel;
	private: System::Windows::Forms::TextBox^ nameField;
	private: System::Windows::Forms::Label^ dateRegisterLabel;
	private: System::Windows::Forms::TextBox^ dateRegisterField;
	private: System::Windows::Forms::Button^ applyButton;
	private: System::Windows::Forms::Label^ notifyLabel;







	protected:

	private:
		unsigned long int _ID;

		   /// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->usernameField = (gcnew System::Windows::Forms::TextBox());
			this->usernameLabel = (gcnew System::Windows::Forms::Label());
			this->positionLabel = (gcnew System::Windows::Forms::Label());
			this->positionField = (gcnew System::Windows::Forms::TextBox());
			this->levelLabel = (gcnew System::Windows::Forms::Label());
			this->lavelField = (gcnew System::Windows::Forms::TextBox());
			this->passLabel = (gcnew System::Windows::Forms::Label());
			this->passField = (gcnew System::Windows::Forms::TextBox());
			this->nameLabel = (gcnew System::Windows::Forms::Label());
			this->nameField = (gcnew System::Windows::Forms::TextBox());
			this->dateRegisterLabel = (gcnew System::Windows::Forms::Label());
			this->dateRegisterField = (gcnew System::Windows::Forms::TextBox());
			this->applyButton = (gcnew System::Windows::Forms::Button());
			this->notifyLabel = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// usernameField
			// 
			this->usernameField->Location = System::Drawing::Point(30, 48);
			this->usernameField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->usernameField->Name = L"usernameField";
			this->usernameField->Size = System::Drawing::Size(202, 25);
			this->usernameField->TabIndex = 0;
			this->usernameField->TextChanged += gcnew System::EventHandler(this, &EditorForm::textBox1_TextChanged);
			// 
			// usernameLabel
			// 
			this->usernameLabel->AutoSize = true;
			this->usernameLabel->Location = System::Drawing::Point(27, 22);
			this->usernameLabel->Name = L"usernameLabel";
			this->usernameLabel->Size = System::Drawing::Size(124, 18);
			this->usernameLabel->TabIndex = 1;
			this->usernameLabel->Text = L"��� ������������";
			this->usernameLabel->Click += gcnew System::EventHandler(this, &EditorForm::label1_Click);
			// 
			// positionLabel
			// 
			this->positionLabel->AutoSize = true;
			this->positionLabel->Location = System::Drawing::Point(27, 91);
			this->positionLabel->Name = L"positionLabel";
			this->positionLabel->Size = System::Drawing::Size(78, 18);
			this->positionLabel->TabIndex = 3;
			this->positionLabel->Text = L"���������";
			// 
			// positionField
			// 
			this->positionField->Location = System::Drawing::Point(30, 117);
			this->positionField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->positionField->Name = L"positionField";
			this->positionField->Size = System::Drawing::Size(202, 25);
			this->positionField->TabIndex = 2;
			// 
			// levelLabel
			// 
			this->levelLabel->AutoSize = true;
			this->levelLabel->Location = System::Drawing::Point(27, 165);
			this->levelLabel->Name = L"levelLabel";
			this->levelLabel->Size = System::Drawing::Size(114, 18);
			this->levelLabel->TabIndex = 5;
			this->levelLabel->Text = L"������� �������";
			// 
			// lavelField
			// 
			this->lavelField->Location = System::Drawing::Point(30, 191);
			this->lavelField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->lavelField->Name = L"lavelField";
			this->lavelField->Size = System::Drawing::Size(202, 25);
			this->lavelField->TabIndex = 4;
			// 
			// passLabel
			// 
			this->passLabel->AutoSize = true;
			this->passLabel->Location = System::Drawing::Point(265, 22);
			this->passLabel->Name = L"passLabel";
			this->passLabel->Size = System::Drawing::Size(55, 18);
			this->passLabel->TabIndex = 9;
			this->passLabel->Text = L"������";
			// 
			// passField
			// 
			this->passField->Location = System::Drawing::Point(268, 48);
			this->passField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->passField->Name = L"passField";
			this->passField->Size = System::Drawing::Size(202, 25);
			this->passField->TabIndex = 8;
			// 
			// nameLabel
			// 
			this->nameLabel->AutoSize = true;
			this->nameLabel->Location = System::Drawing::Point(265, 91);
			this->nameLabel->Name = L"nameLabel";
			this->nameLabel->Size = System::Drawing::Size(38, 18);
			this->nameLabel->TabIndex = 11;
			this->nameLabel->Text = L"���";
			// 
			// nameField
			// 
			this->nameField->Location = System::Drawing::Point(268, 117);
			this->nameField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->nameField->Name = L"nameField";
			this->nameField->Size = System::Drawing::Size(202, 25);
			this->nameField->TabIndex = 10;
			// 
			// dateRegisterLabel
			// 
			this->dateRegisterLabel->AutoSize = true;
			this->dateRegisterLabel->Location = System::Drawing::Point(265, 165);
			this->dateRegisterLabel->Name = L"dateRegisterLabel";
			this->dateRegisterLabel->Size = System::Drawing::Size(120, 18);
			this->dateRegisterLabel->TabIndex = 13;
			this->dateRegisterLabel->Text = L"���� �����������";
			// 
			// dateRegisterField
			// 
			this->dateRegisterField->Location = System::Drawing::Point(268, 191);
			this->dateRegisterField->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->dateRegisterField->Name = L"dateRegisterField";
			this->dateRegisterField->Size = System::Drawing::Size(202, 25);
			this->dateRegisterField->TabIndex = 12;
			// 
			// applyButton
			// 
			this->applyButton->Location = System::Drawing::Point(30, 233);
			this->applyButton->Name = L"applyButton";
			this->applyButton->Size = System::Drawing::Size(111, 33);
			this->applyButton->TabIndex = 14;
			this->applyButton->Text = L"���������";
			this->applyButton->UseVisualStyleBackColor = true;
			this->applyButton->Click += gcnew System::EventHandler(this, &EditorForm::applyButton_Click);
			// 
			// notifyLabel
			// 
			this->notifyLabel->AutoSize = true;
			this->notifyLabel->Location = System::Drawing::Point(27, 279);
			this->notifyLabel->Name = L"notifyLabel";
			this->notifyLabel->Size = System::Drawing::Size(0, 18);
			this->notifyLabel->TabIndex = 15;
			this->notifyLabel->Visible = false;
			// 
			// EditorForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 18);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(508, 323);
			this->Controls->Add(this->notifyLabel);
			this->Controls->Add(this->applyButton);
			this->Controls->Add(this->dateRegisterLabel);
			this->Controls->Add(this->dateRegisterField);
			this->Controls->Add(this->nameLabel);
			this->Controls->Add(this->nameField);
			this->Controls->Add(this->passLabel);
			this->Controls->Add(this->passField);
			this->Controls->Add(this->levelLabel);
			this->Controls->Add(this->lavelField);
			this->Controls->Add(this->positionLabel);
			this->Controls->Add(this->positionField);
			this->Controls->Add(this->usernameLabel);
			this->Controls->Add(this->usernameField);
			this->Font = (gcnew System::Drawing::Font(L"Open Sans", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"EditorForm";
			this->Text = L"EditorForm";
			this->Load += gcnew System::EventHandler(this, &EditorForm::EditorForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void textBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void EditorForm_Load(System::Object^ sender, System::EventArgs^ e) {
		DataBase<User> users(userFile);
		char* ID = new char[512]{};
		User* user = users.Find(string("ID"), _ID);
		if (user) {
			this->usernameField->Text = gcnew String(user->GetData<string>("username").c_str());
			this->passField->Text = gcnew String(user->GetData<string>("password").c_str());
			this->nameField->Text = gcnew String(user->GetData<string>("name").c_str());
			this->dateRegisterField->Text = gcnew String(user->GetData<string>("date register").c_str());
			this->positionField->Text = gcnew String(user->GetData<string>("position").c_str());
			this->lavelField->Text = Convert::ToString(user->GetData<int>("level"));
		}
		else {
			this->notifyLabel->Visible = true;
			this->notifyLabel->Text = gcnew String("������������ �� ������");
		}
	}
	private: System::Void applyButton_Click(System::Object^ sender, System::EventArgs^ e) {
		DataBase<User> users(userFile);
		string notify{};
		bool result = false;
		User* user = users.Find(string("ID"), _ID);
		string username = convertSysStringToChar(this->usernameField->Text);
		if (!username.empty()) {
			if (!(users.Find(string("username"), username)) || user->GetData<string>(string("username")) == username) {
				string password = convertSysStringToChar(this->passField->Text);
				if (!password.empty()) {
					string name = convertSysStringToChar(this->nameField->Text);
					if (!name.empty()) {
						int level = Convert::ToInt16(this->lavelField->Text);
						if (level == 0 || level == 1) {
							string dateRegister = convertSysStringToChar(this->dateRegisterField->Text);
							if (!dateRegister.empty()) {
								string position = convertSysStringToChar(this->positionField->Text);
								user->Set(username, password, name, position, level, dateRegister);
								notify = "������ ������������ ��������";
								result = true;
							}
							else {
								notify = "������� ���� �����������";
							}
						}
						else {
							notify = "�������� ������ ������� ������ ���� 0 ��� 1";
						}
					}
					else {
						notify = "������� ���";
					}
				}
				else {
					notify = "������� ������";
				}
			}
			else {
				notify = "������������ � ����� ������� ��� ����������";
			}
		}
		else {
			notify = "������� ��� ������������";
		}
		this->notifyLabel->Text = gcnew String(notify.c_str());
		this->notifyLabel->Visible = true;
		if (result) {
			users.Save();
		}
	}
};
}
