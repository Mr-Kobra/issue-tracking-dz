#pragma once
#include "LetterForm/LetterForm.h"

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� UserPanel
	/// </summary>
	public ref class UserPanel : public System::Windows::Forms::Form
	{
	public:
		UserPanel(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~UserPanel()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^ mailList;
	protected:
	private: System::Windows::Forms::Button^ readButton;

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->mailList = (gcnew System::Windows::Forms::ListBox());
			this->readButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// mailList
			// 
			this->mailList->FormattingEnabled = true;
			this->mailList->ItemHeight = 27;
			this->mailList->Items->AddRange(gcnew cli::array< System::Object^  >(1) { L"���� ������" });
			this->mailList->Location = System::Drawing::Point(14, 17);
			this->mailList->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->mailList->Name = L"mailList";
			this->mailList->Size = System::Drawing::Size(208, 328);
			this->mailList->TabIndex = 0;
			this->mailList->SelectedIndexChanged += gcnew System::EventHandler(this, &UserPanel::mailList_SelectedIndexChanged);
			// 
			// readButton
			// 
			this->readButton->Enabled = false;
			this->readButton->Location = System::Drawing::Point(241, 17);
			this->readButton->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->readButton->Name = L"readButton";
			this->readButton->Size = System::Drawing::Size(87, 32);
			this->readButton->TabIndex = 1;
			this->readButton->Text = L"������";
			this->readButton->UseVisualStyleBackColor = true;
			this->readButton->Click += gcnew System::EventHandler(this, &UserPanel::readButton_Click);
			// 
			// UserPanel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 27);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(346, 361);
			this->Controls->Add(this->readButton);
			this->Controls->Add(this->mailList);
			this->Font = (gcnew System::Drawing::Font(L"Open Sans", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"UserPanel";
			this->Text = L"������ ������������";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void readButton_Click(System::Object^ sender, System::EventArgs^ e) {
		State::LetterForm^ letterForm = gcnew LetterForm();
		letterForm->ShowDialog(this);
	}
	private: System::Void mailList_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		this->readButton->Enabled = true;
	}
	};
}
