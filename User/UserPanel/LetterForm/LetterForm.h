#pragma once

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� LetterForm
	/// </summary>
	public ref class LetterForm : public System::Windows::Forms::Form
	{
	public:
		LetterForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~LetterForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ fromLabel;
	private: System::Windows::Forms::Label^ authorLabel;
	private: System::Windows::Forms::Label^ dateSendLabel;
	private: System::Windows::Forms::Label^ dateSendValue;
	private: System::Windows::Forms::Label^ messageLabel;
	private: System::Windows::Forms::RichTextBox^ messageValue;

	protected:


	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->fromLabel = (gcnew System::Windows::Forms::Label());
			this->authorLabel = (gcnew System::Windows::Forms::Label());
			this->dateSendLabel = (gcnew System::Windows::Forms::Label());
			this->dateSendValue = (gcnew System::Windows::Forms::Label());
			this->messageLabel = (gcnew System::Windows::Forms::Label());
			this->messageValue = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// fromLabel
			// 
			this->fromLabel->AutoSize = true;
			this->fromLabel->Location = System::Drawing::Point(26, 23);
			this->fromLabel->Name = L"fromLabel";
			this->fromLabel->Size = System::Drawing::Size(30, 18);
			this->fromLabel->TabIndex = 0;
			this->fromLabel->Text = L"��: ";
			// 
			// authorLabel
			// 
			this->authorLabel->AutoSize = true;
			this->authorLabel->Location = System::Drawing::Point(62, 23);
			this->authorLabel->Name = L"authorLabel";
			this->authorLabel->Size = System::Drawing::Size(179, 18);
			this->authorLabel->TabIndex = 1;
			this->authorLabel->Text = L"������ ������� ��������";
			// 
			// dateSendLabel
			// 
			this->dateSendLabel->AutoSize = true;
			this->dateSendLabel->Location = System::Drawing::Point(26, 50);
			this->dateSendLabel->Name = L"dateSendLabel";
			this->dateSendLabel->Size = System::Drawing::Size(105, 18);
			this->dateSendLabel->TabIndex = 2;
			this->dateSendLabel->Text = L"���� ��������: ";
			// 
			// dateSendValue
			// 
			this->dateSendValue->AutoSize = true;
			this->dateSendValue->Location = System::Drawing::Point(137, 50);
			this->dateSendValue->Name = L"dateSendValue";
			this->dateSendValue->Size = System::Drawing::Size(72, 18);
			this->dateSendValue->TabIndex = 3;
			this->dateSendValue->Text = L"2021-05-13";
			// 
			// messageLabel
			// 
			this->messageLabel->AutoSize = true;
			this->messageLabel->Location = System::Drawing::Point(26, 79);
			this->messageLabel->Name = L"messageLabel";
			this->messageLabel->Size = System::Drawing::Size(85, 18);
			this->messageLabel->TabIndex = 4;
			this->messageLabel->Text = L"���������:";
			// 
			// messageValue
			// 
			this->messageValue->Location = System::Drawing::Point(29, 109);
			this->messageValue->Name = L"messageValue";
			this->messageValue->ReadOnly = true;
			this->messageValue->Size = System::Drawing::Size(270, 224);
			this->messageValue->TabIndex = 5;
			this->messageValue->Text = L"Lorem Ipsum - ��� �����-\"����\", ����� ������������ � ������ � ���-�������.";
			// 
			// LetterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 18);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(331, 361);
			this->Controls->Add(this->messageValue);
			this->Controls->Add(this->messageLabel);
			this->Controls->Add(this->dateSendValue);
			this->Controls->Add(this->dateSendLabel);
			this->Controls->Add(this->authorLabel);
			this->Controls->Add(this->fromLabel);
			this->Font = (gcnew System::Drawing::Font(L"Open Sans", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"LetterForm";
			this->Text = L"������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}
