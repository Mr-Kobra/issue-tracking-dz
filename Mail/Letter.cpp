#include "Letter.h"
#include "../DataBase/DataBase.h"
#include "../Global/Global.h"
#include <vector>

using namespace std;

Letter::Letter()
{
	_ID = 0;
	_userID = 0;
	_title = string();
	_body = string();
	_sendDate = string();
}

Letter::Letter(unsigned long int userID, string title, string body, string sendDate) {
	DataBase<Letter> letters(string("DataBase/Files/Users.txt"));
	_ID = letters.GetMaxID() + 1;
	_userID = userID;
	_title = title;
	_body = body;
	_sendDate = sendDate;
}

Letter::Letter(string dataString)
{
	vector<string> result = Splite(dataString, ';');
	if (result.size() == 5) {
		_ID = atoi(result[0].c_str());
		_userID = atoi(result[1].c_str());
		_title = result[2];
		_body = result[3];
		_sendDate = result[4];
	}
}

template <>
string Letter::GetData(string fieldName)
{
	string result{};
	if (fieldName == "title") {
		result = _title;
	} else if(fieldName == "body") {
		result = _body;
	}
	else if (fieldName == "send date") {
		result = _sendDate;
	}
	return result;
}

template <>
unsigned long int Letter::GetData(string fieldName)
{
	unsigned long int result = 0;
	if (fieldName == "ID") {
		result = _ID;
	}
	else if (fieldName == "user ID") {
		result = _userID;
	}
	return result;
}

template <>
void Letter::SetData(string fieldName, string metaData) 
{
	if (fieldName == "title") {
		_title = metaData;
	}
	else if (fieldName == "body") {
		_body = metaData;
	}
	else if (fieldName == "send date") {
		_sendDate = metaData;
	}
}

template<>
void Letter::SetData(string fieldName, unsigned long int metaData)
{
	if (fieldName == "user ID") {
		_userID = metaData;
	}
}

string Letter::GetDataString()
{
	string result{};
	char* ID = new char[256]{};
	char* userID = new char[256]{};
	sprintf_s(ID, 256, "%d", _ID);
	sprintf_s(userID, 256, "%d", _userID);
	result = result + ID + ";";
	result = result + userID + ";";
	result = result + _title + ";";
	result = result + _body + ";";
	result = result + _sendDate + ";";
	return result;
}