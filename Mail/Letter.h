#pragma once
#include <string>
using namespace std;

class Letter {
public:
	Letter();
	Letter(unsigned long int userID, string title, string body, string sendDate);
	Letter(string dataString);
	template <typename T>
	T GetData(string fieldName);
	template <typename T>
	void SetData(string fieldName, T metaData);
	string GetDataString();
private:
	unsigned long int _ID;
	unsigned long int _userID;
	string _title;
	string _body;
	string _sendDate;
};